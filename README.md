# Unify Technical

The project models (in very broad strokes) the data model of a Unify Phone System, in which a collection of Accounts each owns a collection of Phone Numbers which they are allowed to call. Further, the Phone System itself contains a single additional collection of Allowed Numbers which any account is allowed to call (globally allowed numbers).

The model and a static instance of it are herein contained, alongside a RESTful API controller which interrogates the model in delivering it's responses.

Once downloaded and executed, inspect the code and observe that there are existing API endpoints along the following lines:

http://localhost:5000/phonesystem - to see the entire data model
http://localhost:5000/phonesystem/accounts - to see all accounts and their associated properties

Sign in (or up) to Gitlab in order to proceed.
Clone the project, and create a gitlab issue and branch specific to this assignment.

1. Add support to the project for Swagger API documentation.

2. Observe the method GetAccount(string PIN) in PhoneSystemController which is not yet implemented.
- Implement the method to return the account (if any) with a PIN number matching the supplied GET variable.
- Ensure that the method behaves appropriately (i.e. in line with the expectations of an API user) in the event of no match being found.

3. Observe the method ValidateNumber(string PIN, string Digits) in PhoneSystemController which is not yet implemented. The method is intended to return PersonalAllowedNumber or GlobalAllowedNumber objects with Digits matching those supplied by the Digits parameter.
- Receive the Digits parameter as a body-encoded variable.
- Define a response schema to oblige the intended return value.
- Consult the data model and return appropriate response.

Push your code to the previously created branch and create a merge request!
