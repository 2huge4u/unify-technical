﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace UnifyTechnical.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PhoneSystemController : ControllerBase
    {
        private static readonly PhoneSystem _data = new()
        {
            Name = "Unify Business Solutions",
            Accounts = new()
            {
                new Account()
                {
                    PIN = "717788",
                    Name = "Sam Coates",
                    AllowedNumbers = new()
                    {
                        new PersonalAllowedNumber()
                        {
                            Name = "Adrian Hand",
                            Digits = "01246811882"
                        },
                        new PersonalAllowedNumber()
                        {
                            Name = "Phil McCracken",
                            Digits = "07816516043"
                        }
                    }
                },
                new Account()
                {
                    PIN = "987987",
                    Name = "Nick Murray",
                    AllowedNumbers = new()
                    {
                        new PersonalAllowedNumber()
                        {
                            Name = "Eddie Bird",
                            Digits = "01246811882"
                        },
                        new PersonalAllowedNumber()
                        {
                            Name = "Adam Esposito",
                            Digits = "07909474821"
                        }
                    }
                }
            },
            GlobalNumbers = new()
            {
                new GlobalAllowedNumber()
                {
                    Name = "Samaritans",
                    Digits = "116123"
                }
            }
        };

        private readonly ILogger<PhoneSystemController> _logger;

        public PhoneSystemController(ILogger<PhoneSystemController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetPhoneSystem()
        {
            return Ok(_data);
        }

        [HttpGet("accounts")]
        public IActionResult GetAccounts()
        {
            return Ok(_data.Accounts);
        }

        [HttpGet("accounts/{pin}")]
        public IActionResult GetAccount(string PIN)
        {
            throw new NotImplementedException();
        }

        [HttpGet("accounts/{pin}/validatenumber")]
        public IActionResult ValidateNumber(string PIN, string Digits)
        {
            throw new NotImplementedException();
        }
    }
}
