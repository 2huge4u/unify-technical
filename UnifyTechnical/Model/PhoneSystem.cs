using System.Collections.Generic;

namespace UnifyTechnical
{
    public class PhoneSystem
    {
        public string Name { get; set; }
        public List<Account> Accounts { get; set; }
        public List<GlobalAllowedNumber> GlobalNumbers { get; set; }
    }
}