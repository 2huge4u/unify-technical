using System.Collections.Generic;

namespace UnifyTechnical
{
    public class Account
    {
        public string PIN { get; set; }
        public string Name { get; set; }
        public List<PersonalAllowedNumber> AllowedNumbers { get; set; }
    }
}